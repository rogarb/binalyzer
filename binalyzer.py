#!/usr/bin/env python3

import argparse
import magic as libmagic
import multiprocessing as mp
from multiprocessing import shared_memory
import os
import queue
import struct
import sys
import textwrap
import threading

version_string = "Version 0.12"
# the "code" entry contains the first 2 characters of the format string provided to
# struct.unpack()
input_formats = {
    "byte": {"desc": "data is coded with 1 byte", "code": "=B"},
    "u16-le": {
        "desc": "data is coded with 2 bytes in little endian order",
        "code": "<H",
    },
    "u16-be": {
        "desc": "data is coded with 2 bytes in big endian order",
        "code": ">H",
    },
    "u32-le": {
        "desc": "data is coded with 4 bytes in little endian order",
        "code": "<I",
    },
    "u32-be": {
        "desc": "data is coded with 4 bytes in big endian order",
        "code": ">I",
    },
}
output_formats = {
    "ascii": {
        "desc": "Safe ASCII mode, only show printable chars on screen",
        "formatter": lambda x, len: f"{x:c}" if x >= 0x20 and x <= 0x7E else ".",
    },
    "ext_ascii": {
        "desc": "Extended ASCII mode, show printable characters on screen or their (pythonic) hex representation",
        "formatter": lambda x, len: f"{x:c}"
        if x >= 0x20 and x <= 0x7E
        else repr(x.to_bytes(len, byteorder=sys.byteorder).decode()).strip("'"),
    },
    "hex": {
        "desc": "Hex mode, print hex values",
        "formatter": lambda x, len: ("0x{{:0{}x}} ".format(2 * len)).format(x),
    },
}

# available sets and their corresponding testing functions for the strings command
sets = {
    "ascii": {
        "desc": "Any ASCII character",
        "test_function": lambda x: x.isascii(),
    },
    "printable": {
        "desc": "Any printable ASCII character",
        "test_function": lambda x: x.isprintable(),
    },
    "ascii_printable": {
        "desc": "Any printable ASCII character",
        "test_function": lambda x: x.isascii() and x.isprintable(),
    },
    "alpha": {
        "desc": "ASCII alphabetic characters",
        "test_function": lambda x: x.isascii() and x.isalpha(),
    },
    "alphanum": {
        "desc": "ASCII alphanumeric characters",
        "test_function": lambda x: x.isascii() and x.isalnum(),
    },
    "digit": {
        "desc": "Digit characters",
        "test_function": lambda x: x.isdigit(),
    },
    "upper": {
        "desc": "Uppercase ASCII alphabetic characters",
        "test_function": lambda x: x.isascii() and x.isalpha() and x.isupper(),
    },
    "lower": {
        "desc": "Lowercase ASCII alphabetic characters",
        "test_function": lambda x: x.isascii() and x.isalpha() and x.islower(),
    },
}

# define queue size
fifo_len = 512


class AddressParser(argparse.Action):
    """An Action class to parse addresses"""

    def __init__(self, option_strings, dest, **kwargs):
        super(AddressParser, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        # argument is in hex format
        if values.lower().startswith("0x") and set(
            values.strip("0xX").lower()
        ).issubset(set("0123456789abcdef")):
            hex_string = values.lower()[2:]
            parsed_value = int(hex_string, base=16)
        # argument is in binary format
        elif values.lower().startswith("0b") and set(values.strip("0b")).issubset(
            set("01")
        ):
            bin_string = values.lower()[2:]
            parsed_value = int(bin_string, base=2)
        # argument is in octal format
        elif values.startswith("0") and set(values).issubset(set("01234567")):
            octal_string = values[1:]
            parsed_value = int(octal_string, base=8)
        # argument is in decimal format
        elif values.isdigit():
            parsed_value = int(values)
        # invalid input
        else:
            sys.exit("Invalid input {}".format(values))
        setattr(namespace, self.dest, parsed_value)


def compute_size(args):
    """Returns the sanitized size according to arguments"""

    size = os.stat(args.input_file).st_size
    print(f"Input file size: {size}")
    if args.size is not None:
        size = args.size
    elif args.to is not None:
        if args.to <= args.start:
            sys.exit("Error: argument --from must be lesser that argument --to")
        size = args.to - args.start
    elif args.start != 0:
        if size <= args.start:
            sys.exit("Error: argument --from must be lesser that the file size")
        size = size - args.start
    return size


def get_bytes(filename, format, offset, size):
    """Returns the requeste bytes from filename using struct.unpack"""
    with open(filename, "rb") as f:
        f.seek(offset)
        bytes = f.read(size)

    obj_order = format[0]
    obj_repr = format[1]
    obj_len = struct.calcsize(obj_repr)
    # Check that input length is consistent with input format
    modulo = size % obj_len
    if modulo != 0:
        size = size - modulo
        print(
            f"""Warning: input size is not consistent with input format. 
            Discarding {modulo} bytes and reading {size} bytes"""
        )

    format = "".join([obj_order] + list(obj_repr for i in range(0, size // obj_len)))
    unpacked = struct.unpack(format, bytes[:size])
    return unpacked


# used in threaded version of dump and strings
def fill_queue(queue, event, filename, format, offset, size):
    """Fills the queue with the requeste bytes from filename using struct.unpack"""
    with open(filename, "rb") as f:
        obj_len = struct.calcsize(format[1])
        # Check that input length is consistent with input format
        modulo = size % obj_len
        if modulo != 0:
            size = size - modulo
            print(
                f"""Warning: input size is not consistent with input format. 
                Discarding {modulo} bytes and reading {size} bytes"""
            )

        f.seek(offset)
        # improve performances by reading the bytes necessary to obtain
        # queue.maxsize elements
        remaining = size
        while remaining > 0:
            to_read = min(remaining, obj_len * queue.maxsize)
            bytes = f.read(to_read)
            # as we fill one object at a time, but unpack returns a tuple (a
            # singleton in this case), "flatten" it
            for i in range(0, len(bytes), obj_len):
                queue.put(struct.unpack(format, bytes[i : i + obj_len])[0])
            remaining -= len(bytes)
        event.set()
        # queue.join()


def dump(args):
    """Function executed by the dump command"""
    size = compute_size(args)
    filesize = os.stat(args.input_file).st_size

    print(f"Reading {size} bytes of input starting from {args.start}")
    # threaded reading of the data
    q = queue.Queue(fifo_len)
    data_read = threading.Event()
    producer_thread = threading.Thread(
        target=fill_queue,
        kwargs={
            "queue": q,
            "event": data_read,
            "filename": args.input_file,
            "format": input_formats[args.input_format]["code"],
            "offset": args.start,
            "size": size,
        },
        daemon=True,
    )
    producer_thread.start()

    # Set the address format out of the file size: this way the offset printed
    # on screen always contains the same number of digits and the data is aligned
    address_format = "0x{{:0{}x}}: ".format(len("{:x}".format(filesize)))
    address_len = len(address_format.format(0))

    # Set up variables used to parse the raw input
    obj_len = struct.calcsize(input_formats[args.input_format]["code"][1])
    obj_formatter = output_formats[args.output_format]["formatter"]

    # Calculate the width of the data to display on screen
    # This allows hanling piping and stdout redirection, where os.get_terminal_size()
    # raises an Exception, if and only --number *and* width flags are not specified
    if args.number is not None:
        char_width = len(obj_formatter(0, obj_len))
        width = args.number * char_width + address_len
    elif args.width is not None:
        width = args.width
    else:
        try:
            (term_width, _) = os.get_terminal_size()
            width = term_width
        except:
            sys.exit("Unable to get terminal width. Try using --width or --number flag")

    output = list("")
    line_len = 0
    tmp = None
    i = 0
    while True:
        if q.empty() and data_read.is_set():
            print("".join(output))
            break
        else:
            # print(f"DBG: {len(b)=}")
            b = q.get()
            output_element = ""
            try:
                output_element = obj_formatter(b, obj_len)
            except Exception as e:
                sys.exit("[ERROR] {}".format(e))

            if line_len + len(output_element) > width:
                print("".join(output))
                line_len = 0
                output.clear()
            if line_len == 0:
                # Correct relative offset to absolute
                address = address_format.format(i * obj_len + args.start)
                output.append(address)
                line_len += address_len
            output.append(output_element)
            line_len += len(output_element)
            i += 1
            q.task_done()

    producer_thread.join()


def extract(args):
    """This function is executed by the extract command"""
    if not os.path.exists(args.input_file):
        sys.exit("Error: input file {} doesn't exist".format(args.input_file))

    size = compute_size(args)
    if os.path.exists(args.output):
        if os.path.isdir(args.output):
            # an existing dir cannot be written as a file, exiting
            sys.exit("Error: {} is a directory".format(args.output))
        else:
            if args.force:
                print("Warning: file {} already exists. Overwriting as --force flag was provided".format(args.output))
            else:
                sys.exit("Error: file {} already exists".format(args.output))
    print("Extracting {} bytes from 0x{:x}".format(size, args.start))
    with open(args.input_file, "rb") as input_file:
        input_file.seek(args.start)
        with open(args.output, "wb") as output_file:
            output_file.write(input_file.read(size))
    print("Done")


# Data class for dealing with threaded magic scanning
from dataclasses import dataclass, field
from typing import Any

@dataclass(order=True)
class MagicResult:
    address: int
    description: Any=field(compare=False)

def magic_scan_thread(data, offset):
# def magic_scan_thread(data, offset, start):
    """A worker function for magic bytes scanning

    @data: bytes to scan
    @offset: offset from the beginning of the bytes /!\ it is not checked against the buffer size /!\ 
    @q: the queue where to send the results
    @lock: the queue lock shared among all threads
    @start: the initial value of the data in the file (for result printing purposes)"""

    res = libmagic.from_buffer(data[offset:])
    if res != "data":
        return MagicResult(address = offset, description = res)


def magic(args):
    """Function executed by the magic command

    It uses the python magic module"""
    size = compute_size(args)
    filesize = os.stat(args.input_file).st_size
    if args.scan:
        if args.threaded:
            pool_size = 20
            chunksize = 1024

            print(f"Scanning {size} bytes from 0x{args.start:#x}")
            
            results = []
            shm = shared_memory.SharedMemory(create=True,size=size)
            data = shm.buf
            with open(args.input_file, "rb") as f:
                f.seek(args.start)
                data = f.read(size)
            
            with mp.Pool(pool_size) as threads :
                results = threads.starmap(magic_scan_thread, list((data, i) for i in range(0, size)), chunksize)
                threads.close()
                threads.join()
            shm.close()
            shm.unlink()
            results.sort()
            if len(results) != 0:
                for item in results:
                    print(f"{args.start + item.address:#x}: {item.description}")
            else:
                print("No specific file type found")
        else:
            offset = 0
            found = False
            print(f"Scanning {size} bytes from 0x{args.start:#x}")
            while offset < size:
                with open(args.input_file, "rb") as f:
                    f.seek(args.start + offset)
                    res = libmagic.from_buffer(f.read(size - offset))
                    if res != "data":
                        found = True
                        print(f"{args.start + offset:#x}: {res}")
                offset += 1
            if not found:
                print("No specific file type found")
    else:
        print(f"Checking {size} bytes from 0x{args.start:#X}")
        with open(args.input_file, "rb") as f:
            f.seek(args.start)
            print(f"{args.start:#x}: ", libmagic.from_buffer(f.read(size)))


def search(args):
    """Function executed by the search command"""
    if args.file is not None:
        with open(args.file, "rb") as f:
            data = f.read()
    # args.data is not None
    else:
        # expect hex formatted data
        if args.data.lower().startswith("0x"):
            in_data = args.data[2:]
            if len(args.data) % 2 != 0:
                in_data = "0" + in_data
            data = bytes.fromhex(in_data)
        # assume data is a string
        else:
            data = args.data.encode()

    filesize = os.stat(args.input_file).st_size
    size = len(data)
    position = 0
    count = 0
    print(f"Searching {size} bytes of data in {args.input_file}")
    with open(args.input_file, "rb") as f:
        while position + size <= filesize:
            f.seek(position)
            read_data = f.read(size)
            if data == read_data:
                print(f"Found data at address {position:#x}")
                count += 1
                if args.count is not None and count >= args.count:
                    break
            position += 1
        if count == 0:
            print("Data not found in input file")


def strings(args):
    """Function executed by the strings command"""
    contentsize = compute_size(args)
    # threaded reading of the data
    q = queue.Queue(fifo_len)
    data_read = threading.Event()
    filesize = os.stat(args.input_file).st_size
    producer_thread = threading.Thread(
        target=fill_queue,
        kwargs={
            "queue": q,
            "event": data_read,
            "filename": args.input_file,
            "format": input_formats[args.input_format]["code"],
            "offset": args.start,
            "size": contentsize,
        },
        daemon=True,
    )
    producer_thread.start()

    address_format = "0x{{:0{}x}}: ".format(len("{:x}".format(filesize)))
    test_function = sets[args.set]["test_function"]
    address = args.start
    size = args.min_size
    s = ""
    while True:
        if q.empty() and data_read.is_set():
            if len(s) >= args.min_size:
                print(address_format.format(address) + s)
            break
        else:
            b = q.get()
            q.task_done()
            # when using u32 format, the conversion generally raises a ValueError or
            # an OverflowError, meaning the data is not a valid character
            try:
                c = chr(b)
            except:
                address += 1
                continue
            else:
                if test_function(c):
                    s += c
                else:
                    if len(s) >= args.min_size:
                        print(address_format.format(address) + s)
                    address += len(s)
                    s = ""
            # building of a string has not started yet, the address 
            # pointer is incremented
            if len(s) == 0:
                address += 1
    producer_thread.join()


# Script implementation
if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description="A simple tool to analyze a binary file",
    )
    parser.add_argument("-V", "--version", action="version", version=version_string)
    subparsers = parser.add_subparsers(title="commands", required=True, dest="command")

    # Dump/print subcommand
    dump_command = subparsers.add_parser(
        "dump",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        help="Print input file content to the screen",
        description=textwrap.dedent(
            "INPUT_FORMAT can be one of these values:\n"
            + "".join(
                "  - " + k + ": " + v["desc"] + "\n" for k, v in input_formats.items()
            )
            + "\nOUPUT_FORMAT can be one of these values:\n"
            + "".join(
                "  - " + k + ": " + v["desc"] + "\n" for k, v in output_formats.items()
            )
        ),
        aliases=["print"],
    )
    dump_command.set_defaults(func=dump)
    dump_command.add_argument("input_file", help="File containing the data to analyze")
    dump_command.add_argument(
        "-F",
        "--format",
        help="Treat input data as %(metavar)s",
        metavar="INPUT_FORMAT",
        choices=input_formats.keys(),
        dest="input_format",
        default="byte",
        required=False,
    )
    dump_command.add_argument(
        "-o",
        "--output",
        metavar="OUTPUT_FORMAT",
        dest="output_format",
        choices=output_formats,
        default="ascii",
        help="Set output format, see below (default: ascii)",
        required=False,
    )
    dump_command.add_argument(
        "-f",
        "--from",
        action=AddressParser,
        required=False,
        metavar="ADDRESS",
        dest="start",
        default=0,
        help="Read data from %(metavar)s (default: beginning of the input file)",
    )
    # Output format related arguments
    output_format_args = dump_command.add_mutually_exclusive_group()
    output_format_args.add_argument(
        "-w",
        "--width",
        required=False,
        help="Display at max WIDTH characters on the screen (default: screen width)",
        metavar="WIDTH",
        type=int,
        default=None,
    )
    output_format_args.add_argument(
        "-n",
        "--number",
        required=False,
        help="Display N values per line",
        metavar="N",
        type=int,
        default=None,
    )
    # Input length related arguments
    len_args = dump_command.add_mutually_exclusive_group()
    len_args.add_argument(
        "-t",
        "--to",
        action=AddressParser,
        metavar="ADDRESS",
        required=False,
        help="Read data until %(metavar)s (default: end of the input file",
    )
    len_args.add_argument(
        "-s",
        "--size",
        action=AddressParser,
        metavar="SIZE",
        required=False,
        help="Read %(metavar)s bytes of data (default: size of the input file)",
    )
    # End of dump/print subcommand

    # extract command
    extract_command = subparsers.add_parser(
        "extract",
        help="Extract the selected bytes to a file",
    )
    extract_command.set_defaults(func=extract)
    extract_command.add_argument(
        "input_file", metavar="INPUT_FILE", help="File to search extract in"
    )
    extract_command.add_argument(
        "-o",
        "--output",
        help="Name of the output file",
        required=True,
        metavar="OUTPUT_FILE",
    )
    extract_command.add_argument(
        "--force",
        action="store_true",
        required=False,
        default=False,
        help="Overwrite the output file if it exists",
    )
    extract_command.add_argument(
        "-f",
        "--from",
        action=AddressParser,
        required=True,
        metavar="ADDRESS",
        dest="start",
        default=0,
        help="Read data from %(metavar)s",
    )
    # Input length related arguments
    extract_len_args = extract_command.add_mutually_exclusive_group(required=True)
    extract_len_args.add_argument(
        "-t",
        "--to",
        action=AddressParser,
        metavar="ADDRESS",
        help="Read data until %(metavar)s",
    )
    extract_len_args.add_argument(
        "-s",
        "--size",
        action=AddressParser,
        metavar="SIZE",
        help="Read %(metavar)s bytes of data",
    )
    # end of extract subcommand

    # magic subcommand
    magic_command = subparsers.add_parser(
        "magic",
        help="Search for magic bytes in a file",
    )
    magic_command.set_defaults(func=magic)
    magic_command.add_argument(
        "input_file", help="file to magic data in", metavar="INPUT_FILE"
    )
    magic_command.add_argument(
        "-S",
        "--scan",
        action="store_true",
        required=False,
        default=False,
        help="Scan the entire data region",
    )
    magic_command.add_argument(
        "-T",
        "--threaded",
        action="store_true",
        required=False,
        default=False,
        help="Use threaded algorithm when scanning",
    )
    magic_command.add_argument(
        "-f",
        "--from",
        action=AddressParser,
        required=False,
        metavar="ADDRESS",
        dest="start",
        default=0,
        help="Read data from %(metavar)s (default: beginning of the input file)",
    )
    # Input length related arguments
    magic_len_args = magic_command.add_mutually_exclusive_group()
    magic_len_args.add_argument(
        "-t",
        "--to",
        action=AddressParser,
        metavar="ADDRESS",
        required=False,
        help="Read data until %(metavar)s (default: end of the input file",
    )
    magic_len_args.add_argument(
        "-s",
        "--size",
        action=AddressParser,
        metavar="SIZE",
        required=False,
        help="Read %(metavar)s bytes of data (default: size of the input file)",
    )
    # End of magic subcommand

    # search subcommand
    search_command = subparsers.add_parser(
        "search",
        help="Search data with input",
    )
    search_command.set_defaults(func=search)
    search_command.add_argument(
        "input_file", help="file to search data in", metavar="INPUT_FILE"
    )
    search_command.add_argument(
        "-c",
        "--count",
        required=False,
        type=int,
        help="maximum number of results to show. If not set, all occurences are shown", 
        metavar="N"
    )
    # Search data argument group
    search_data = search_command.add_mutually_exclusive_group(required=True)
    search_data.add_argument(
        "-f",
        "--file",
        help="Read search data from FILE",
        metavar="FILE",
    )
    search_data.add_argument(
        "-d",
        "--data",
        metavar="DATA",
        help="Read search data from the command line",
    )
    # end of search subcommand

    # strings command
    strings_command = subparsers.add_parser(
        "strings",
        help="Display the strings contained in the input file.",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(
            "SET can be one of these values:\n"
            + "".join("  - " + k + ": " + v["desc"] + "\n" for k, v in sets.items())
            + "\nINPUT_FORMAT can be one of these values:\n"
            + "".join(
                "  - " + k + ": " + v["desc"] + "\n" for k, v in input_formats.items()
            )
        ),
    )
    strings_command.set_defaults(func=strings)
    strings_command.add_argument(
        "input_file", metavar="INPUT_FILE", help="File to search strings in"
    )
    strings_command.add_argument(
        "-S",
        "--set",
        help="Set of characters to accept as a string (default: printable)",
        required=False,
        metavar="SET",
        default="printable",
        choices=sets.keys(),
    )
    strings_command.add_argument(
        "-n",
        "--min-num",
        help="Minimal number of characters to consider (default: 4)",
        required=False,
        metavar="N",
        dest="min_size",
        type=int,
        default=4,
    )
    strings_command.add_argument(
        "-F",
        "--format",
        help="Treat input data as %(metavar)s (default: byte)",
        metavar="INPUT_FORMAT",
        choices=input_formats.keys(),
        dest="input_format",
        default="byte",
        required=False,
    )
    strings_command.add_argument(
        "-f",
        "--from",
        action=AddressParser,
        required=False,
        metavar="ADDRESS",
        dest="start",
        default=0,
        help="Read data from %(metavar)s (default: beginning of the input file)",
    )
    # Input length related arguments
    strings_len_args = strings_command.add_mutually_exclusive_group()
    strings_len_args.add_argument(
        "-t",
        "--to",
        action=AddressParser,
        metavar="ADDRESS",
        required=False,
        help="Read data until %(metavar)s (default: end of the input file",
    )
    strings_len_args.add_argument(
        "-s",
        "--size",
        action=AddressParser,
        metavar="SIZE",
        required=False,
        help="Read %(metavar)s bytes of data (default: size of the input file)",
    )
    # end of strings subcommand

    args = parser.parse_args()
    args.func(args)
