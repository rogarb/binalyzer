binalyzer: a simple script for analyzing binary files
====

Features:
------------
 * Prints the content of a file in binary mode after decoding it.
 * Searches for data inside a binary file (binary grep)
 * Prints the strings contained in a file (supports ASCII, UTF-8, UTF-16 and 
   UTF-32 encodings)
 * Supports 1, 2 or 4-byte encoded input, little-endian or big-endian
 * Supports various output modes when printing file content: ASCII (safe/extended),
   hex
 * Supports filtering different string types: ascii, alphanumeric, alphabetic, 
   digits...

Installation:
-------------
Copy `binalyzer.py` somewhere in your path or use `install.py` script. 
See `$ ./install.py --help`.

Usage:
------
See `$ ./binalyzer.py --help`.
