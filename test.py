#!/usr/bin/env python3

from io import StringIO
from unittest.mock import patch
import binalyzer
import os
import queue
import tempfile
import unittest
import sys


def outtest(q):
    output = ""
    while not q.empty():
        c = q.get()
        output += c
        q.task_done()
    print(output)


def fill_queue(queue, event, filename, format, offset, size):
    for c in filename:
        queue.put(c)


@patch("sys.stdout", new_callable=StringIO)
class DumpCommandTests(unittest.TestCase):
    def setUp(self):
        f = tempfile.NamedTemporaryFile(delete=False)
        f.write(
            bytes("0123456789ABCDEF", encoding="utf8")
            + bytes(0x01 for i in range(0, 16))
        )
        self.args = unittest.mock.MagicMock
        self.args.width = 120
        self.args.input_file = f.name
        self.args.size = 12
        self.args.to = None
        self.args.start = 0
        self.args.input_format = "byte"
        self.args.output_format = "ascii"
        self.args.number = None
        self.queue = queue.Queue()

    def fill_queue(self, string):
        for c in string:
            self.queue.put(c)

    def test(self, mock_stdout):
        self.args.size = 6
        self.args.to = None
        self.args.start = 10
        binalyzer.dump(self.args)
        self.assertEqual(
            mock_stdout.getvalue(),
            "Input file size: {}\nReading {} bytes of input starting from {}\n0x0a: ABCDEF\n".format(
                os.stat(self.args.input_file).st_size, self.args.size, self.args.start
            ),
        )

    def test2(self, mock_stdout):
        binalyzer.dump(self.args)
        self.assertEqual(
            mock_stdout.getvalue(),
            "Input file size: {}\nReading {} bytes of input starting from {}\n0x00: 0123456789AB\n".format(
                os.stat(self.args.input_file).st_size, self.args.size, self.args.start
            ),
        )

    def test3(self, mock_stdout):
        self.args.size = 16
        self.args.to = None
        self.args.start = 16
        binalyzer.dump(self.args)
        self.assertEqual(
            mock_stdout.getvalue(),
            "Input file size: {}\nReading {} bytes of input starting from {}\n0x10: ................\n".format(
                os.stat(self.args.input_file).st_size, self.args.size, self.args.start
            ),
        )


@patch("sys.stdout", new_callable=StringIO)
class StringsCommandTests(unittest.TestCase):
    def setUp(self):
        f = tempfile.NamedTemporaryFile(delete=False)
        f.write(
            bytes("00AA00BB00AA00BB", encoding="utf8")
            + bytes(0x01 for i in range(0, 16))
            + bytes("00AA00BB00AA00BB", encoding="utf8")
        )
        self.args = unittest.mock.MagicMock
        self.args.input_file = f.name
        self.args.min_size = 5
        self.args.size = 12
        self.args.to = None
        self.args.start = 0
        self.args.input_format = "byte"
        self.args.number = None
        self.args.set = "ascii"
        self.queue = queue.Queue()

    def fill_queue(self, string):
        for c in string:
            self.queue.put(c)

    def test(self, mock_stdout):
        self.args.min_size = 6
        self.args.to = None
        self.args.start = 10
        self.args.size = None
        self.args.set = "ascii_printable"
        binalyzer.strings(self.args)
        self.assertEqual(
            mock_stdout.getvalue(),
            "Input file size: {}\n0x0a: AA00BB\n0x20: 00AA00BB00AA00BB\n".format(
                os.stat(self.args.input_file).st_size
            ),
        )

    def test2(self, mock_stdout):
        binalyzer.strings(self.args)
        self.assertEqual(
            mock_stdout.getvalue(),
            "Input file size: {}\n0x00: 00AA00BB00AA\n".format(
                os.stat(self.args.input_file).st_size
            ),
        )

    def test3(self, mock_stdout):
        self.args.min_size = 16
        self.args.to = None
        self.args.start = 16
        self.args.size = None
        self.args.set = "ascii_printable"
        binalyzer.strings(self.args)
        self.assertEqual(
            mock_stdout.getvalue(),
            "Input file size: {}\n0x20: 00AA00BB00AA00BB\n".format(
                os.stat(self.args.input_file).st_size
            ),
        )

    def test4(self, mock_stdout):
        self.args.min_size = 16
        self.args.to = None
        self.args.start = 16
        self.args.size = None
        binalyzer.strings(self.args)
        self.assertEqual(
            mock_stdout.getvalue(),
            "Input file size: {}\n0x10: \x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x01\x0100AA00BB00AA00BB\n".format(
                os.stat(self.args.input_file).st_size
            ),
        )


if __name__ == "__main__":
    unittest.main()
