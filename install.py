#!/usr/bin/env python3

import argparse
import os
import shutil

source = "binalyzer.py"
destdir = os.getenv(
    "DESTDIR",
    default=os.getenv("HOME", default=os.path.dirname(os.path.realpath(__file__)))
    + "/bin",
)

parser = argparse.ArgumentParser(
    description=f"Install {source} to $DESTDIR (default value: {destdir})"
)
parser.add_argument(
    "-d",
    "--destdir",
    required=False,
    help="Specify destination folder",
    default=destdir,
)
parser.add_argument(
    "-f", "--force", action="store_true", required=False, help="Overwrite target"
)
args = parser.parse_args()

target = args.destdir + "/" + source

try:
    import magic
except:
    print(
        """\
python-magic module not found.

{} needs the python-magic module. You can install it through pip:

`$ pip install magic`

or through your package manager, i.e. for Ubuntu/Debian based systems:
 
`# apt install python3-magic`"""
    )
    sys.exit()

if not os.access(target, os.F_OK) or args.force:
    print(f"Copying {source} to {target}")
    try:
        shutil.copy(source, target)
    except Exception as e:
        print(f"Error: {e}")
    else:
        print("Done")
else:
    print(f"{target} already exists: remove or une --force flag")
